# Secure Access to Web Content
## Interview Tech Task

**Goal:** provide secure web access to sensitive content.

The solution is resilient to:
- **Pulling sensitive content from container images**: all sensitive content is mounted at deploy-time so that nothing is leaked if container images are stolen. This includes sensitive plaintext, SSL keys and user credentials.
- **Browsing directly to the container serving sensitive content:** the container housing sensitive data is shielded by a proxy container and is otherwise unaddressable
- **Sniffing cleartext on the wire:** all communication between your browser, the proxy and the data is protected by TLS encryption over HTTPS
- **Stealing either the URL token or the authentication credentials:** to access sensitive data, the solution requires both a successful user login and knowledge of the secret token
- **Filesystem discovery:** all invalid URLs return the 404 not found page so that hackers can't discover directories that exist on the sensitive data container
- **Use of insecure TLS protocols:** TLS 1.0 and older SSL protocols are insecure and have been disabled, per https://www.globalsign.com/en/blog/disable-tls-10-and-all-ssl-versions/

## Assumptions
- Ports 80 and 443 are available for the containers to bind to
- Tested on Windows 10 and Ubuntu 18.04

## Requirements
- Docker Desktop for Windows or Docker CE for Ubuntu
- `docker-compose`
- Sensitive data file bundle

## Installation
* `git clone https://gitlab.com/turbodog/pa`
* `cd pa`
* If running on Unbuntu, `chmod -R 766 sensitive/*`
* `docker build -t proxy proxy`
* `docker build -t web web`

## Running
* `docker-compose up -d`
* Browse to http://hostname or https://hostname

## Architecture
![Solution Diagram](diagram.png)
* The proxy container listens on the standard HTTP and HTTPS ports
* HTTP is automatically redirected to HTTPS inside the proxy
* The proxy enforces basic auth and HTTPS for communication with the web container
* If basic auth is accepted, HTTPS requests are proxied to the web container
* Web container is not addressable from the Internet or the host OS
* Sensitive content is stored on the Docker host filesystem and mounted at runtime
* The web container serves the sensitive content if an only if the URL is correct
* Otherwise 404s are returned